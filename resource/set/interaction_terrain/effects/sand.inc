;SDL
{"sand"
  {on hit
    {if min_mass 100
      {if effector "tree"
        {spawn "tree_crush"}
      else
        {spawn "spheredust_small"}
      }
    else min_mass 10
      {spawn "spheredust_small_short"}
    }
  }
  {on detonate
    {if stuff "molotov_coctail"
      {spawn "ex_molot"}
      {spawn "fire_molot_ground"}
      {spawn "fire_shellhole"}
    else stuff "grenade big"
      {spawn "ex_grenade_big"}
      {spawn "grenade_at_shellhole" terrain_texmod}
    else stuff "grenade"
      {spawn "ex_grenade_sm"}
      {spawn "grenade_shellhole" terrain_texmod}
    else stuff "small bullet"
      {spawn "ex_bul_sm"}
    else stuff "zenite"
      {spawn "ex_bul_zenit"}
      {spawn "shellhole_small"}
    else stuff "bullet"
      {spawn "ex_bul_big"}
    else stuff "mortar"
      {spawn "ex_mort_big"}
      {spawn "shellhole_big_sand"}
	else stuff "big mortar_smoke"
	  {spawn "mortar_smoke"}
	  {spawn "sand_hit_small"}
    else stuff "mortar"
;	  {spawn "shockwave"}
      {spawn "ex_bomb_big"}
      {spawn "shellhole_big_sand"}
    else stuff "shell size1 fg"
;      {spawn "shockwave"}
      {spawn "ex_anti_size1"}
      {spawn "grenade_shellhole" terrain_texmod}
    else stuff "shell size1"
;      {spawn "shockwave"}
      {spawn "ex_arm_size1"}
;      {spawn "grenade_shellhole"}
    else stuff "shell size2 fg"
;      {spawn "shockwave"}
      {spawn "ex_anti_size2"}
      {spawn "crater_norm" terrain_texmod}
    else stuff "shell size2"
;      {spawn "shockwave"}
      {spawn "ex_arm_size2"}
;      {spawn "shellhole_big_ground"}
    else stuff "shell size3 fg"
;      {spawn "shockwave"}
      {spawn "ex_anti_size3"}
      {spawn "crater_norm" terrain_texmod}
    else stuff "shell size3"
;      {spawn "shockwave"}
      {spawn "ex_arm_size3"}
;      {spawn "shellhole_big_ground"}
    else stuff "shell size4"
;      {spawn "shockwave"}
      {spawn "ex_anti_size4"}
      {spawn "crater_big" terrain_texmod}
    else stuff "ap shell"
      {spawn "ex_grenade_sm"}
    else stuff "big shell 203"
;      {spawn "shockwave"}
      {spawn "ex_bomb_big"}
      {spawn "crater_norm" terrain_texmod}
    else stuff "big shell fg bomb no_crater small"
      {spawn "shockwave"}
      {spawn "ex_anti_big"}
      {spawn "shellhole4d3"}
    else stuff "big shell fg bomb no_crater"
;      {spawn "shockwave"}
      {spawn "ex_bomb_big"}
      {spawn "shellhole4d2"}
    else stuff "big shell fg bomb"
;      {spawn "shockwave"}
      {spawn "ex_bomb_big"}
      {spawn "crater_norm" terrain_texmod}
    else stuff "mine antipersonnel"
	  {spawn "ex_mort_sm"}
      {spawn "shellhole_big_sand"}
    else stuff "mine antitank"
	  {spawn "ex_mort_big"}
      {spawn "shellhole_big_sand"}
	else stuff "mine clockwork"
		{spawn "ex_mort_big"}
		{spawn "shellhole_big_sand"}
    else stuff "dynamite"
;	  {spawn "shockwave"}
      {spawn "ex_bomb_big"}
      {spawn "shellhole_big_sand"}
    else stuff "dynamite_clockwork"
;	  {spawn "shockwave"}
      {spawn "ex_bomb_big"}
      {spawn "shellhole_big_sand"}
    else stuff "tnt_bag"
;	  {spawn "shockwave"}
      {spawn "ex_bomb_big"}
      {spawn "shellhole_big_sand"}
    }
  }
}
