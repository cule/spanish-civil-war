;SDL
{"bridge"
	{on hit
		{spawn "spheredust_small_short"}
	}
	{on detonate
		{if stuff "molotov_coctail"
			{spawn "ex_molot"}
			{spawn "fire_molot_ground"}
		else stuff "grenade big"
			{spawn "ex_grenade_big"}
		else stuff "grenade"
			{spawn "ex_grenade_sm"}
		else stuff "small bullet"
			{spawn "ex_bul_sm"}
		else stuff "zenite"
			{spawn "ex_bul_zenit"}
		else stuff "bullet"
			{spawn "ex_bul_big"}
		else stuff "big mortar"
			{spawn "ex_mort_big"}
		else stuff "mortar_smoke"
			{spawn "mortar_smoke"}
	  		{spawn "spheredust_small_short"}	
		else stuff "mortar"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big"}
	    else stuff "shell size1 fg"
;	      {spawn "shockwave"}
	      {spawn "ex_anti_size1"}
;	      {spawn "grenade_shellhole" terrain_texmod}
	    else stuff "shell size1"
;			{spawn "shockwave"}
			{spawn "ex_arm_size1"}
;			{spawn "grenade_shellhole"}
	    else stuff "shell size2 fg"
			{spawn "shockwave"}
			{spawn "ex_anti_size2"}
	    else stuff "shell size2"
			{spawn "shockwave"}
			{spawn "ex_arm_size2"}
;			{spawn "shellhole_big_ground"}
    	else stuff "shell size3 fg"
			{spawn "shockwave"}
			{spawn "ex_anti_size3"}
	    else stuff "shell size3"
			{spawn "shockwave"}
			{spawn "ex_arm_size3"}
;			{spawn "shellhole_big_ground"}
    	else stuff "shell size4"
			{spawn "shockwave"}
			{spawn "ex_anti_size4"}
	    else stuff "ap shell"
    		{spawn "ex_grenade_sm"}
		else stuff "big shell extra"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big"}
		else stuff "big shell fg bomb"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big"}
		else stuff "mine antipersonnel"
			{spawn "ex_grenade_big"}
		else stuff "mine antitank"
			{spawn "ex_arm_big"}
		else stuff "mine clockwork"
			{spawn "ex_anti_big"}
		else stuff "dynamite"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big"}
		else stuff "dynamite_clockwork"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big"}
		else stuff "tnt_bag"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big"}
		}
	}
}

{"bridge_wood"
	{on hit
		{spawn "spheredust_small_short"}
	}
	{on detonate
		{if stuff "molotov_coctail"
			{spawn "ex_molot"}
			{spawn "fire_molot_ground"}
		else stuff "grenade big"
			{spawn "ex_grenade_big"}
		else stuff "grenade"
			{spawn "ex_grenade_sm"}
		else stuff "small bullet"
			{spawn "ex_bul_sm"}
		else stuff "zenite"
			{spawn "ex_bul_zenit"}
		else stuff "bullet"
			{spawn "wood_hit_bul"}
		else stuff "big mortar"
			{spawn "ex_mort_big_wood"}
		else stuff "mortar"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big_wood"}
		else stuff "big shell extra"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big_wood"}
		else stuff "big shell fg bomb"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big_wood"}
		else stuff "big shell fg"
			{spawn "shockwave"}
			{spawn "ex_anti_big_wood"}
		else stuff "big shell ap"
			{spawn "ex_mort_big_wood"}
		else stuff "small shell fg"
			{spawn "shockwave"}
			{spawn "ex_mort_sm_wood"}
		else stuff "shell fg"
			{spawn "shockwave"}
			{spawn "ex_anti_big_wood"}
		else stuff "shell ap"
			{spawn "ex_mort_sm_wood"}
		else stuff "mine antipersonnel"
			{spawn "ex_grenade_big"}
		else stuff "mine antitank"
			{spawn "ex_mort_big_wood"}
		else stuff "mine clockwork"
			{spawn "ex_anti_big_wood"}
		else stuff "dynamite"
;			{spawn "shockwave"}
			{spawn "ex_bomb_big_wood"}
		}
	}
}
