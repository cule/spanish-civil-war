{"snow"
  {on hit
    {if min_mass 100
      {if effector "tree"
        {spawn "tree_crush"}
      else
        {spawn "spheredust_small"}
      }
    else min_mass 10
      {spawn "spheredust_small_short"}
    }
  }
  {on detonate
    {if stuff "molotov_coctail"
      {spawn "ex_molot"}
      {spawn "fire_molot_ground"}
      {spawn "fire_shellhole"}
    else stuff "grenade big"
      {spawn "ex_grenade_big_snow"}
      {spawn "grenade_at_shellhole" terrain_texmod}
    else stuff "grenade"
      {spawn "ex_grenade_sm_snow"}
      {spawn "grenade_shellhole" terrain_texmod}
    else stuff "small bullet"
      {spawn "ex_bul_sm_snow"}
    else stuff "zenite"
      {spawn "ex_bul_zenit_snow"}
      {spawn "shellhole_small"}
    else stuff "bullet"
      {spawn "ex_bul_big_snow"}
    else stuff "big mortar"
      {spawn "ex_mort_big_snow"}
      {spawn "shellhole_big"}
	else stuff "mortar_smoke"
	  {spawn "mortar_smoke"}
	  {spawn "ex_bul_sm_snow"}
    else stuff "mortar"
;	  {spawn "shockwave"}
      {spawn "ex_bomb_big_snow"}
      {spawn "shellhole_medium"}
    else stuff "shell size1 fg"
;      {spawn "shockwave"}
      {spawn "ex_anti_size1_snow"}
      {spawn "grenade_shellhole" terrain_texmod}
    else stuff "shell size1"
;      {spawn "shockwave"}
      {spawn "ex_arm_size1"}
;      {spawn "grenade_shellhole"}
    else stuff "shell size2 fg"
;      {spawn "shockwave"}
      {spawn "ex_anti_size2_snow"}
      {spawn "crater_norm" terrain_texmod}
    else stuff "shell size2"
;      {spawn "shockwave"}
      {spawn "ex_arm_size2"}
;      {spawn "shellhole_big_ground"}
    else stuff "shell size3 fg"
;      {spawn "shockwave"}
      {spawn "ex_anti_size3_snow"}
      {spawn "crater_norm" terrain_texmod}
    else stuff "shell size3"
;      {spawn "shockwave"}
      {spawn "ex_arm_size3"}
;      {spawn "shellhole_big_ground"}
    else stuff "shell size4"
;      {spawn "shockwave"}
      {spawn "ex_anti_size4_snow"}
      {spawn "crater_big" terrain_texmod}
    else stuff "big shell extra"
;      {spawn "shockwave"}
      {spawn "ex_bomb_big_snow" offset 0 0 20}
      {spawn "crater_norm" terrain_texmod}
    else stuff "big shell fg bomb no_crater small"
      {spawn "shockwave"}
      {spawn "ex_mort_big_snow"}
      {spawn "shellhole6_win1"}
    else stuff "big shell fg bomb no_crater"
;      {spawn "shockwave"}
      {spawn "ex_bomb_big_snow"}
      {spawn "shellhole6_win2"}
    else stuff "big shell fg bomb"
;      {spawn "shockwave"}
      {spawn "ex_bomb_big_snow"}
      {spawn "crater_norm" terrain_texmod}
    else stuff "big shell fg"
      {spawn "shockwave"}
      {spawn "ex_mort_big_snow"}
      {spawn "crater_norm" terrain_texmod}
    else stuff "big shell ap"
      {spawn "ex_mort_sm_snow"}
;      {spawn "shellhole_big_ground"}
    else stuff "small shell fg"
      {spawn "shockwave"}
      {spawn "ex_anti_small"}
      {spawn "snow_hit_big"}
      {spawn "shellhole_big_ground"}
    else stuff "shell fg"
      {spawn "shockwave"}
      {spawn "ex_mort_big_snow"}
      {spawn "crater_norm" terrain_texmod}
    else stuff "shell ap"
      {spawn "ex_arm_sm"}
      {spawn "snow_hit_big"}
;      {spawn "shellhole_big_ground"}
    else stuff "mine antipersonnel"
      {spawn "ex_grenade_big_snow"}
      {spawn "grenade_shellhole" terrain_texmod}
    else stuff "mine antitank"
	  {spawn "ex_mort_sm_snow"}
      {spawn "grenade_at_shellhole" terrain_texmod}
	else stuff "mine clockwork"
		{spawn "ex_mort_big_snow"}
		{spawn "grenade_at_shellhole" terrain_texmod}
    else stuff "dynamite"
;	  {spawn "shockwave"}
      {spawn "ex_bomb_big_snow"}
      {spawn "shellhole_big"}
    else stuff "dynamite_clockwork"
;	  {spawn "shockwave"}
      {spawn "ex_bomb_big_snow"}
      {spawn "shellhole_big"}
    else stuff "tnt_bag"
;	  {spawn "shockwave"}
      {spawn "ex_bomb_big_snow"}
      {spawn "shellhole_big"}
    }
  }
}
