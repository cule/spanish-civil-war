;= HUMAN =====================================

{"tt_tankman"}

{"human"
    ("uncover" u(7))
    ("h_fov" h(70))
    ("v_fov" v(200))
    ("vision_human")
}

{"mp_soldier"
    ("uncover" u(10))
    ("h_fov" h(150))
    ("v_fov" v(200))
    ("vision_human")
}

{"mp_officer"
    ("uncover" u(50))
    ("h_fov" h(180))
    ("v_fov" v(200))
    ("vision_officer")
}
{"mp_mgunner"
    ("uncover" u(10))
    ("h_fov" h(80))
    ("v_fov" v(200))
    ("vision_mgunner")
}
{"mp_sniper"
    ("uncover" u(10))
    ("h_fov" h(100))
    ("v_fov" v(200))
    ("vision_sniper")
}

{"mp_specnaz"
    ("uncover" u(60))
    ("h_fov" h(360))
    ("v_fov" v(200))
    ("vision_stealth")
}
{"human_around"
    ("uncover" u(1))
    ("h_fov" h(200))
    ("v_fov" v(180))
    ("human_stand")
}
;= SPECIAL ===================================

{"soldier_focused"
    ("uncover" u(10))
    ("h_fov" h(30))
    ("v_fov" v(50))
    ("vision_human")
}

{"rifle_telescope"
    ("uncover" u(7))
    ("h_fov" h(360))
    ("v_fov" v(120))
    ("glass" args 7)
}

{"field_glass"
    ("uncover" u(1))
    ("h_fov" h(360))
    ("v_fov" v(120))
    ("glass" args 15)
}

;= CANNON ========================================

{"cannon_main"
    ("uncover" u(5))
    ("h_fov" h(60))
    ("v_fov" v(140))
    ("vision_optic")
}

{"cannon_mgun"
    ("uncover" u(5))
    ("h_fov" h(50))
    ("v_fov" v(140))
    ("vision_human")
;    {wide 2}
}

{"cannon_main-far"
    ("uncover" u(5))
    ("h_fov" h(60))
    ("v_fov" v(140))
    ("vision_optic")
}

{"cannon_crew"
    ("uncover" u(5))
    ("h_fov" h(360))
    ("v_fov" v(140))
    ("human_stand")
}

;= VEHICLE & MOTO ============================

{"vehicle_main"
    ("uncover" u(5))
    ("h_fov" h(50))
    ("v_fov" v(140))
    ("vision_human")
}

{"vehicle_top_mgun"
    ("h_fov" h(90))
    ("v_fov" v(140))
    ("vision_human")
}
{"vehicle_top_turret2"
    ("uncover" u(3))
    ("h_fov" h(100))
    ("v_fov" v(20))
    ("vision_turret")
}
{"vehicle_top_turret"
    ("uncover" u(1))
    ("h_fov" h(360))
    ("v_fov" v(20))
    ("vision_turret")
}
{"moto"
    ("uncover" u(5))
    ("h_fov" h(30))
    ("v_fov" v(140))
    ("vision_human")
}
{"moto_crew"
    ("uncover" u(5))
    ("h_fov" h(90))
    ("v_fov" v(60))
    ("vision_human")
}

{"moto_around"
    ("uncover" u(3))
    ("h_fov" h(360))
    ("v_fov" v(60))
    ("human_stand")
}

;= TANK ======================================
{"mg_hull"
    ("uncover" u(3))
    ("h_fov" h(5))
    ("v_fov" v(90))
    ("vision_tgunner")
}
{"tank_main-l"
    ("uncover" u(5))
    ("h_fov" h(30))
    ("v_fov" v(140))
    ("vision_optic")
}

{"tank_main-m"
    ("uncover" u(5))
    ("h_fov" h(25))
    ("v_fov" v(140))
    ("vision_optic")
}

{"tank_main-h"
    ("uncover" u(5))
    ("h_fov" h(25))
    ("v_fov" v(140))
    ("vision_optic")
}

{"tank_driver"
    ("uncover" u(3))
    ("h_fov" h(50))
    ("v_fov" v(20))
    ("vision_tgunner")
}

{"tank_back"
    ("uncover" u(3))
    ("h_fov" h(50))
    ("v_fov" v(120))
    ("vision_tgunner")
}
{"l_tank_unc"
    ("uncover" u(70))
    ("h_fov" h(25))
    ("v_fov" v(180))
    ("vision_optic_unc")
}
{"tank_around-l"

}

{"tank_around-m"

}

{"tank_around-h"

}

{"none"
}
;===================
{"Visor_Artil"
    ("uncover" u(3))
    ("h_fov" h(15))
    ("v_fov" v(100))
    ("vision_optic")
}

{"Visor_BTR"
    ("uncover" u(10))
    ("h_fov" h(30))
    ("v_fov" v(100))
    ("vision_optic")
}


{"Visor_CAY"
    ("uncover" u(5))
    ("h_fov" h(25))
    ("v_fov" v(180))
    ("vision_optic")
}

{"sau_unc" 
    ("uncover" u(70))
    ("h_fov" h(25))
    ("v_fov" v(180))
    ("vision_optic_unc")
}

{"Visor_Tank_Long"
    ("uncover" u(5))
    ("h_fov" h(25))
    ("v_fov" v(180))
    ("vision_optic")
}

{"Visor_Tank"
    ("uncover" u(5))
    ("h_fov" h(25))
    ("v_fov" v(180))
    ("vision_optic")
}

{"tank_unc"
    ("uncover" u(70))
    ("h_fov" h(25))
    ("v_fov" v(180))
    ("vision_optic_unc")
}

{"Visor_cannon"
    ("uncover" u(5))
    ("h_fov" h(25))
    ("v_fov" v(180))
    ("vision_optic")
}
{"cann_unc"
    ("uncover" u(70))
    ("vision_optic_unc")
}

