;= START =====================================


(define "vision_human"
    ("radius" r(70))
    {rules
        {human 1
            {firing 1.157
                {firing_silent 0.1}
                {firing_delisle 0.2}
                {knife_ger 0.1}
                {knife_rus 0.1}
                {knife_union 0.1}
                {f1 0.1}
                {m18 0.1}
                {m24 0.1}
                {m24x5 0.1}
                {m39 0.1}
                {m61 0.1}
                {mk1 0.1}
                {molotov_coctail 0.1}
                {n73at 0.1}
                {pwm1 0.1}
                {rg42x3 0.1}
                {rpg40 0.1}
                {rpg43 0.1}
                {smoke 0.1}
                {type_3_at 0.1}
                {type_97 0.1}
                {crawl 1.157}
                {cover 1.157}
            }
            {cover 0.15
                {crawl 0.071
                    {sniper 0.014}
                    {stealth 0.014}
                }
            }
            {squat 0.857}
            {crawl 0.62
                {still 0.57
                    {sniper 0.28}
                    {stealth 0.28}
                }
                {sniper 0.35}
                {stealth 0.35}
            }
            {still 0.929}
            {visible 1.143}
        }
        {cannon_01 1
            {firing 1.2}
            {visible 1}
            {still 1}
        }
        {cannon_02 1.1
            {firing 1.3}
            {visible 1.1}
            {still 1.1}
        }
        {cannon_03 1.2
            {firing 1.4}
            {visible 1.2}
            {still 1.2}
        }
        {cannon_04 1.3
            {firing 1.5}
            {visible 1.3}
            {still 1.3}
        }
        {cannon_05 1.4
            {firing 1.6}
            {visible 1.4}
            {still 1.4}
        }
        {vehicle_00 1.1}

        {vehicle_01 1.7}

        {vehicle_02 1.8}

        {vehicle_03 1.9
            {firing 2.2}
            {visible 1.9}
            {still 1.8}
        }
        {vehicle_04 2
            {firing 2.3}
            {visible 2}
            {still 1.9}
        }
        {vehicle_04_spa 2.1
            {firing 2.4}
            {visible 2.1}
            {still 1.9}
        }
        {vehicle_05 2.15
            {firing 2.45}
            {visible 2.15}
            {still 1.95}
        }
        {vehicle_06 2.2
            {firing 2.5}
            {visible 2.2}
            {still 2}
        }
        {vehicle_07 2
            {firing 3}
            {visible 2}
            {still 2}
        }
    }
    {mods
        {stand 1}
    }
)



(define "vision_mgunner"
    ("radius" r(80))
    {rules
        {human 1
            {firing 1.013
                {firing_silent 0.1}
                {firing_delisle 0.2}
                {knife_ger 0.1}
                {knife_rus 0.1}
                {knife_union 0.1}
                {f1 0.1}
                {m18 0.1}
                {m24 0.1}
                {m24x5 0.1}
                {m39 0.1}
                {m61 0.1}
                {mk1 0.1}
                {molotov_coctail 0.1}
                {n73at 0.1}
                {pwm1 0.1}
                {rg42x3 0.1}
                {rpg40 0.1}
                {rpg43 0.1}
                {smoke 0.1}
                {type_3_at 0.1}
                {type_97 0.1}
                {crawl 1.013}
                {cover 1}
            }
            {cover 0.125
                {crawl 0.088
                    {sniper 0.013}
                    {stealth 0.013}
                }
            }
            {squat 0.75}
            {crawl 0.563
                {still 0.5
                    {sniper 0.28}
                    {stealth 0.28}
                }
                {sniper 0.35}
                {stealth 0.35}
            }
            {still 0.938}
            {visible 1}
        }
        {cannon_01 0.75
            {firing 0.888}
            {visible 0.875}
            {still 0.75}
        }
        {cannon_02 0.875
            {firing 1.063}
            {visible 1}
            {still 0.813}
        }
        {cannon_03 0.938
            {firing 1.125}
            {visible 1.063}
            {still 0.875}
        }
        {cannon_04 1
            {firing 1.188}
            {visible 1.125}
            {still 0.938}
        }
        {cannon_05 1.063
            {firing 1.25}
            {visible 1.188}
            {still 1}
        }
        {vehicle_00 0.75}

        {vehicle_01 0.75}

        {vehicle_02 1}

        {vehicle_03 1.125
            {firing 1.25}
            {visible 1.188}
            {still 1}
        }
        {vehicle_04 1.125
            {firing 1.25}
            {visible 1.188}
            {still 1}
        }
        {vehicle_04_spa 1.375
            {firing 1.625}
            {visible 1.563}
            {still 1}
        }
        {vehicle_05 1.375
            {firing 1.625}
            {visible 1.563}
            {still 1.125}
        }
        {vehicle_06 1.5
            {firing 1.75}
            {visible 1.688}
            {still 1.25}
        }
        {vehicle_07 1.125
            {firing 2.5}
            {visible 1.625}
            {still 1}
        }
    }
    {mods
        {stand 1}
    }
)


(define "vision_officer"
    ("radius" r(80))
    {rules
        {human 1
            {firing 1.5
                {firing_silent 0.1}
                {firing_delisle 0.2}
                {knife_ger 0.1}
                {knife_rus 0.1}
                {knife_union 0.1}
                {f1 0.1}
                {m18 0.1}
                {m24 0.1}
                {m24x5 0.1}
                {m39 0.1}
                {m61 0.1}
                {mk1 0.1}
                {molotov_coctail 0.1}
                {n73at 0.1}
                {pwm1 0.1}
                {rg42x3 0.1}
                {rpg40 0.1}
                {rpg43 0.1}
                {smoke 0.1}
                {type_3_at 0.1}
                {type_97 0.1}
                {crawl 1.5}
                {cover 1.5}
            }
            {cover 0.375
                {crawl 0.375
                    {sniper 0.375}
                    {stealth 0.375}
                }
            }
            {squat 0.875}
            {crawl 0.75
                {still 0.75
                    {sniper 0.625}
                    {stealth 0.625}
                }
                {sniper 0.625}
                {stealth 0.625}
            }
            {still 0.938}
            {visible 1.5}
        }
        {cannon_01 0.875
            {firing 1.013}
            {visible 1}
            {still 0.875}
        }
        {cannon_02 1
            {firing 1.188}
            {visible 1.125}
            {still 0.938}
        }
        {cannon_03 1.063
            {firing 1.25}
            {visible 1.188}
            {still 1}
        }
        {cannon_04 1.125
            {firing 1.313}
            {visible 1.25}
            {still 1.063}
        }
        {cannon_05 1.188
            {firing 1.375}
            {visible 1.313}
            {still 1.125}
        }
        {vehicle_00 0.875}

        {vehicle_01 0.875}

        {vehicle_02 1.125}

        {vehicle_03 1.25
            {firing 1.375}
            {visible 1.313}
            {still 1.125}
        }
        {vehicle_04 1.25
            {firing 1.375}
            {visible 1.313}
            {still 1.125}
        }
        {vehicle_04_spa 1.5
            {firing 1.75}
            {visible 1.688}
            {still 1.125}
        }
        {vehicle_05 1.5
            {firing 1.75}
            {visible 1.688}
            {still 1.25}
        }
        {vehicle_06 1.625
            {firing 1.875}
            {visible 1.813}
            {still 1.375}
        }
        {vehicle_07 1.25
            {firing 2.5}
            {visible 1.625}
            {still 1.25}
        }
    }
    {mods
        {stand 1}
    }
)


(define "vision_stealth"
    ("radius" r(80))
    {rules
        {human 1
            {firing 1.5
                {firing_silent 0.1}
                {firing_delisle 0.2}
                {knife_ger 0.1}
                {knife_rus 0.1}
                {knife_union 0.1}
                {f1 0.1}
                {m18 0.1}
                {m24 0.1}
                {m24x5 0.1}
                {m39 0.1}
                {m61 0.1}
                {mk1 0.1}
                {molotov_coctail 0.1}
                {n73at 0.1}
                {pwm1 0.1}
                {rg42x3 0.1}
                {rpg40 0.1}
                {rpg43 0.1}
                {smoke 0.1}
                {type_3_at 0.1}
                {type_97 0.1}
                {crawl 1.5}
                {cover 1.5}
            }
            {cover 0.5
                {crawl 0.5
                    {sniper 0.013}
                    {stealth 0.013}
                }
            }
            {squat 0.875}
            {crawl 0.75
                {still 0.75
                    {sniper 0.625}
                    {stealth 0.625}
                }
                {sniper 0.625}
                {stealth 0.625}
            }
            {still 1}
            {visible 1.5}
        }
        {cannon_01 1
            {firing 1.138}
            {visible 1.125}
            {still 1}
        }
        {cannon_02 1.125
            {firing 1.313}
            {visible 1.25}
            {still 1.063}
        }
        {cannon_03 1.188
            {firing 1.375}
            {visible 1.313}
            {still 1.125}
        }
        {cannon_04 1.25
            {firing 1.438}
            {visible 1.375}
            {still 1.188}
        }
        {cannon_05 1.313
            {firing 1.5}
            {visible 1.438}
            {still 1.25}
        }
        {vehicle_00 1}

        {vehicle_01 1}

        {vehicle_02 1.25}

        {vehicle_03 1.375
            {firing 1.5}
            {visible 1.438}
            {still 1.25}
        }
        {vehicle_04 1.375
            {firing 1.5}
            {visible 1.438}
            {still 1.25}
        }
        {vehicle_04_spa 1.625
            {firing 1.875}
            {visible 1.813}
            {still 1.25}
        }
        {vehicle_05 1.625
            {firing 1.875}
            {visible 1.813}
            {still 1.375}
        }
        {vehicle_06 1.75
            {firing 2}
            {visible 1.938}
            {still 1.5}
        }
        {vehicle_07 1.75
            {firing 2.5}
            {visible 1.875}
            {still 1.75}
        }
    }
    {mods
        {stand 1}
    }
)

(define "vision_sniper"
    ("radius" r(100))
    {rules
        {human 1
            {firing 1.2
                {firing_silent 0.1}
                {firing_delisle 0.2}
                {knife_ger 0.1}
                {knife_rus 0.1}
                {knife_union 0.1}
                {f1 0.1}
                {m18 0.1}
                {m24 0.1}
                {m24x5 0.1}
                {m39 0.1}
                {m61 0.1}
                {mk1 0.1}
                {molotov_coctail 0.1}
                {n73at 0.1}
                {pwm1 0.1}
                {rg42x3 0.1}
                {rpg40 0.1}
                {rpg43 0.1}
                {smoke 0.1}
                {type_3_at 0.1}
                {type_97 0.1}
                {crawl 1.2}
                {cover 1.2}
            }
            {cover 0.1
                {crawl 0.07
                    {sniper 0.01}
                    {stealth 0.01}
                }
            }
            {squat 0.6}
            {crawl 0.45
                {still 0.4
                    {sniper 0.25}
                    {stealth 0.25}
                }
                {sniper 0.3}
                {stealth 0.3}
            }
            {still 0.9}
            {visible 1.2}
        }
        {cannon_01 0.6
            {firing 0.71}
            {visible 0.7}
            {still 0.6}
        }
        {cannon_02 0.7
            {firing 0.85}
            {visible 0.8}
            {still 0.65}
        }
        {cannon_03 0.75
            {firing 0.9}
            {visible 0.85}
            {still 0.7}
        }
        {cannon_04 0.8
            {firing 0.95}
            {visible 0.9}
            {still 0.75}
        }
        {cannon_05 0.85
            {firing 1}
            {visible 0.95}
            {still 0.8}
        }
        {vehicle_00 0.6}

        {vehicle_01 0.6}

        {vehicle_02 0.8}

        {vehicle_03 0.9
            {firing 1}
            {visible 0.95}
            {still 0.8}
        }
        {vehicle_04 0.9
            {firing 1}
            {visible 0.95}
            {still 0.8}
        }
        {vehicle_04_spa 1.1
            {firing 1.3}
            {visible 1.25}
            {still 0.8}
        }
        {vehicle_05 1.1
            {firing 1.3}
            {visible 1.25}
            {still 0.9}
        }
        {vehicle_06 1.2
            {firing 1.4}
            {visible 1.35}
            {still 1}
        }
        {vehicle_07 0.9
            {firing 2}
            {visible 1.3}
            {still 0.8}
        }
    }
    {mods
        {stand 1}
    }
)

(define "vision_optic"
    ("radius" r(90))
    {rules
        {human 0.75
            {firing 0.75
                {firing_silent 0.1}
                {firing_delisle 0.2}
                {knife_ger 0.1}
                {knife_rus 0.1}
                {knife_union 0.1}
                {f1 0.1}
                {m18 0.1}
                {m24 0.1}
                {m24x5 0.1}
                {m39 0.1}
                {m61 0.1}
                {mk1 0.1}
                {molotov_coctail 0.1}
                {n73at 0.1}
                {pwm1 0.1}
                {rg42x3 0.1}
                {rpg40 0.1}
                {rpg43 0.1}
                {smoke 0.1}
                {type_3_at 0.1}
                {type_97 0.1}
                {crawl 0.4}
                {cover 0.1}
            }
            {cover 0.125
                {crawl 0.088
                    {sniper 0.013}
                    {stealth 0.013}
                }
            }
            {squat 0.7}
            {crawl 0.55
                {still 0.55
                    {sniper 0.1}
                    {stealth 0.1}
                }
                {sniper 0.1}
                {stealth 0.1}
            }
            {still 0.75}
            {visible 0.75}
        }
        {cannon_01 0.875
            {firing 1.013}
            {visible 1}
            {still 0.875}
        }
        {cannon_02 1.5
            {firing 2.5}
            {visible 2.5}
            {still 1.375}
        }
        {cannon_03 1.75
            {firing 2.5}
            {visible 2.5}
            {still 1.75}
        }
        {cannon_04 2
            {firing 2.5}
            {visible 2.5}
            {still 1.875}
        }
        {cannon_05 1.25
            {firing 2.5}
            {visible 2.5}
            {still 1.25}
        }
        {vehicle_00 1.125}

        {vehicle_01 1.25}

        {vehicle_02 1.625}

        {vehicle_03 2
            {firing 2.5}
            {visible 2.5}
            {still 1.875}
        }
        {vehicle_04 2
            {firing 2.5}
            {visible 2.5}
            {still 1.875}
        }
        {vehicle_04_spa 2.25
            {firing 2.5}
            {visible 2.5}
            {still 2}
        }
        {vehicle_05 2.25
            {firing 2.5}
            {visible 2.5}
            {still 2.125}
        }
        {vehicle_06 2.5
            {firing 2.625}
            {visible 2.5}
            {still 2.375}
        }
        {vehicle_07 1.875
            {firing 3.75}
            {visible 1.875}
            {still 1.625}
        }
    }
    {mods
        {stand 1}
    }
)








(define "vision_tgunner"
    ("radius" r(40))
    {rules
        {human 1
            {firing 1
                {firing_silent 0.1}
                {firing_delisle 0.2}
                {knife_ger 0.1}
                {knife_rus 0.1}
                {knife_union 0.1}
                {f1 0.1}
                {m18 0.1}
                {m24 0.1}
                {m24x5 0.1}
                {m39 0.1}
                {m61 0.1}
                {mk1 0.1}
                {molotov_coctail 0.1}
                {n73at 0.1}
                {pwm1 0.1}
                {rg42x3 0.1}
                {rpg40 0.1}
                {rpg43 0.1}
                {smoke 0.1}
                {type_3_at 0.1}
                {type_97 0.1}
                {crawl 1}
                {cover 1}
            }
            {cover 0.025
                {crawl 0.025
                    {sniper 0.025}
                    {stealth 0.025}
                }
            }
            {squat 1}
            {crawl 0.625
                {still 0.625
                    {sniper 0.025}
                    {stealth 0.025}
                }
                {sniper 0.025}
                {stealth 0.025}
            }
            {still 1}
            {visible 1}
        }
        {cannon_01 1.5
            {firing 1.775}
            {visible 1.75}
            {still 1.5}
        }
        {cannon_02 1.75
            {firing 2.125}
            {visible 2}
            {still 1.625}
        }
        {cannon_03 1.875
            {firing 2.25}
            {visible 2.125}
            {still 1.75}
        }
        {cannon_04 2
            {firing 2.375}
            {visible 2.25}
            {still 1.875}
        }
        {cannon_05 2.125
            {firing 2.5}
            {visible 2.375}
            {still 2}
        }
        {vehicle_00 1.5}

        {vehicle_01 1.5}

        {vehicle_02 2}

        {vehicle_03 2.25
            {firing 2.5}
            {visible 2.375}
            {still 2}
        }
        {vehicle_04 2.25
            {firing 2.5}
            {visible 2.375}
            {still 2}
        }
        {vehicle_04_spa 2.75
            {firing 3.25}
            {visible 3.125}
            {still 2}
        }
        {vehicle_05 2.75
            {firing 3.25}
            {visible 3.125}
            {still 2.25}
        }
        {vehicle_06 3
            {firing 3.5}
            {visible 3.375}
            {still 2.5}
        }
        {vehicle_07 2.25
            {firing 5}
            {visible 3.25}
            {still 2}
        }
    }
    {mods
        {stand 1}
    }
)



(define "vision_tankman"
	("radius" r(0))
	{rules
		{human 1
            {firing 1
                {firing_silent 0.1}
				{firing_delisle 0.2}
				{knife_ger 0.1}
				{knife_rus 0.1}
				{knife_union 0.1}
				{f1 0.1}
				{m18 0.1}
				{m24 0.1}
				{m24x5 0.1}
				{m39 0.1}
				{m61 0.1}
				{mk1 0.1}
				{molotov_coctail 0.1}
				{n73at 0.1}
				{pwm1 0.1}
				{rg42x3 0.1}
				{rpg40 0.1}
				{rpg43 0.1}
				{smoke 0.1}
				{type_3_at 0.1}
				{type_97 0.1}
            }
        }

        {zero 0}
		{cannon_01 1}
		{cannon_02 1}
		{cannon_03 1}
		{cannon_04 1}
		{cannon_05 1}
		{vehicle_00 1}
		{vehicle_01 1}
		{vehicle_02 1}
		{vehicle_03 1}
		{vehicle_04 1}
		{vehicle_04_spa 1}
		{vehicle_05 1}
		{vehicle_06 1}
		{vehicle_07 1}
	}
)

(define "glass"
		("radius" r(%0))
        {rules
            {zero 0}
        }
)

(define "human_stand"
    ("radius" r(40))
    {rules
        {human 1
            {firing 1.125
                {firing_silent 0.1}
                {firing_delisle 0.2}
                {knife_ger 0.1}
                {knife_rus 0.1}
                {knife_union 0.1}
                {f1 0.1}
                {m18 0.1}
                {m24 0.1}
                {m24x5 0.1}
                {m39 0.1}
                {m61 0.1}
                {mk1 0.1}
                {molotov_coctail 0.1}
                {n73at 0.1}
                {pwm1 0.1}
                {rg42x3 0.1}
                {rpg40 0.1}
                {rpg43 0.1}
                {smoke 0.1}
                {type_3_at 0.1}
                {type_97 0.1}
                {crawl 1}
                {cover 1}
            }
            {cover 0.025
                {crawl 0.025
                    {sniper 0.025}
                    {stealth 0.025}
                }
            }
            {squat 0.025}
            {crawl 0.025
                {still 0.025
                    {sniper 0.025}
                    {stealth 0.025}
                }
                {sniper 0.025}
                {stealth 0.025}
            }
            {still 0.875}
            {visible 0.875}
        }
        {cannon_01 1.5
            {firing 1.775}
            {visible 1.75}
            {still 1.5}
        }
        {cannon_02 1.75
            {firing 2.125}
            {visible 2}
            {still 1.625}
        }
        {cannon_03 1.875
            {firing 2.25}
            {visible 2.125}
            {still 1.75}
        }
        {cannon_04 2
            {firing 2.375}
            {visible 2.25}
            {still 1.875}
        }
        {cannon_05 2.125
            {firing 2.5}
            {visible 2.375}
            {still 2}
        }
        {vehicle_00 1.5}

        {vehicle_01 1.5}

        {vehicle_02 2}

        {vehicle_03 2.25
            {firing 2.5}
            {visible 2.375}
            {still 2}
        }
        {vehicle_04 2.25
            {firing 2.5}
            {visible 2.375}
            {still 2}
        }
        {vehicle_04_spa 2.75
            {firing 3.25}
            {visible 3.125}
            {still 2}
        }
        {vehicle_05 2.75
            {firing 3.25}
            {visible 3.125}
            {still 2.25}
        }
        {vehicle_06 3
            {firing 3.5}
            {visible 3.375}
            {still 2.5}
        }
        {vehicle_07 2.25
            {firing 5}
            {visible 3.25}
            {still 2}
        }
    }
    {mods
        {stand 1}
    }
)


(define "vision_optic_unc"
    ("radius" r(100))
    {rules
        {human 0.1}
        {cannon_01 1}
        {cannon_02 1}
        {cannon_03 1}
        {cannon_04 1}
        {cannon_05 1}
        {vehicle_00 1}
        {vehicle_01 1}
        {vehicle_02 1}
        {vehicle_03 1}

        {vehicle_04 1.714
            {firing 1.857}
            {visible 1.786}
            {still 1.571}
        }
        {vehicle_04_spa 2
            {firing 2.429}
            {visible 2.357}
            {still 1.714}
        }
        {vehicle_05 2
            {firing 2.429}
            {visible 2.357}
            {still 1.857}
        }
        {vehicle_06 2.286
            {firing 2.571}
            {visible 2.5}
            {still 2}
        }
        {vehicle_07 1.286
            {firing 2.857}
            {visible 1.857}
            {still 1.143}
        }
    }
    {mods
        {stand 1}
    }
)


(define "vision_turret"
    ("radius" r(40))
    {rules
        {human 1
            {firing 1
                {firing_silent 0.1}
                {firing_delisle 0.2}
                {knife_ger 0.1}
                {knife_rus 0.1}
                {knife_union 0.1}
                {f1 0.1}
                {m18 0.1}
                {m24 0.1}
                {m24x5 0.1}
                {m39 0.1}
                {m61 0.1}
                {mk1 0.1}
                {molotov_coctail 0.1}
                {n73at 0.1}
                {pwm1 0.1}
                {rg42x3 0.1}
                {rpg40 0.1}
                {rpg43 0.1}
                {smoke 0.1}
                {type_3_at 0.1}
                {type_97 0.1}
                {crawl 1}
                {cover 1}
            }
            {cover 0.025
                {crawl 0.025
                    {sniper 0.025}
                    {stealth 0.025}
                }
            }
            {squat 0.75}
            {crawl 0.025
                {still 0.025
                    {sniper 0.025}
                    {stealth 0.025}
                }
                {sniper 0.025}
                {stealth 0.025}
            }
            {still 0.875}
            {visible 1}
        }
        {cannon_01 1.75
            {firing 2.025}
            {visible 2}
            {still 1.75}
        }
        {cannon_02 2
            {firing 2.75}
            {visible 2.75}
            {still 2}
        }
        {cannon_03 2.75
            {firing 3.25}
            {visible 3.25}
            {still 2.5}
        }
        {cannon_04 3
            {firing 3.75}
            {visible 3.75}
            {still 2.75}
        }
        {cannon_05 2.5
            {firing 2.5}
            {visible 2.375}
            {still 2.5}
        }
        {vehicle_00 2.25}

        {vehicle_01 2.25}

        {vehicle_02 2.5}

        {vehicle_03 3.5
            {firing 3.25}
            {visible 3.25}
            {still 3.25}
        }
        {vehicle_04 3.5
            {firing 3.25}
            {visible 3.25}
            {still 3.25}
        }
        {vehicle_04_spa 3.25
            {firing 3.25}
            {visible 3.25}
            {still 3.25}
        }
        {vehicle_05 3.5
            {firing 3.5}
            {visible 3.5}
            {still 3.5}
        }
        {vehicle_06 3.75
            {firing 3.75}
            {visible 3.75}
            {still 3.75}
        }
        {vehicle_07 2.25
            {firing 5}
            {visible 3.25}
            {still 2}
        }
    }
    {mods
        {stand 1}
    }
)







{"cover-detector"
	{uncover 5}
	{h_fov 60}
	{v_fov 120}
	("vision_optic" args 50)
}

{"animal"
	{uncover 2}
	{h_fov 360}
	{v_fov 120}
}

{"searchlight"
	("vision_optic" args 60)
	{h_fov 30}
	{v_fov 90}
}

{"watchtower"
    ("vision_optic" args 50)
	{h_fov 90}
	{v_fov 90}
}

{"marine_main"
    ("vision_optic" args 24)
	{h_fov 50}
	{v_fov 120}
}

;= MAP HELPER ====================================

(define "ultra_vision"
	{radius 20}
	{h_fov 360}
   	{v_fov 180}
	{uncover %range}
)
{"start_point"  ("ultra_vision" range(20))}
{"map_point"    ("ultra_vision" range(20))}
{"flag_point"   ("ultra_vision" range(20))}
