(include "generic.inc")

;= HUMAN =====================================

{"human"
	{uncover 3}
	("vision_human" args 40)
	{h_fov 70}
	{v_fov 90}
}

{"mp_soldier"
	{uncover 40}
	("vision_human" args 35)
	{h_fov 90}
	{v_fov 120}
}

{"mp_officer"
	{uncover 60}
	("vision_officer" args 55)
	{h_fov 120}
	{v_fov 120}
}

{"mp_sniper"
	{uncover 60}
	("vision_human" args 55) 
	{h_fov 120}
	{v_fov 120}
}

{"mp_specnaz"
	{uncover 60}
	("vision_human" args 55)
	{h_fov 230}
	{v_fov 120}
}


;= SPECIAL ===================================

{"soldier_focused"
	{uncover 65}
	("vision_optic" args 60)
	{v_fov 120}
	{h_fov 270}
}

{"rifle_telescope"
	{uncover 80}
	("glass" args 75)
	{h_fov 360}
	{v_fov 120}
}

{"field_glass"
	{uncover 75}
	("glass" args 70)
	{h_fov 360}
	{v_fov 120}
}

;= CANNON ========================================

{"cannon_main"
    ("vision_optic" args 50)
	{h_fov 60}
	{v_fov 140}
}

{"cannon_mgun"
    ("vision_human" args 45)
	{h_fov 90}
	{v_fov 140}
}

{"cannon_main-far"
    ("vision_optic" args 50)
	{h_fov 60}
	{v_fov 140}
}

{"cannon_crew"
    ("vision_optic" args 35)
	{h_fov 240}
	{v_fov 140}
}

;= VEHICLE & MOTO ============================

{"vehicle_main"
	("vision_optic" args 35)
	{h_fov 90}
	{v_fov 140}
}

{"vehicle_top_mgun"
    ("vision_human" args 35)
	{h_fov 90}
	{v_fov 140}
}

{"vehicle_top_turret"
    ("vision_optic" args 50)
	{h_fov 40}
	{v_fov 140}
}

{"moto_crew"
    ("vision_optic" args 50)
	{h_fov 100}
	{v_fov 60}
}

{"moto_around"
    ("human_stand" args 20)
	{h_fov 360}
	{v_fov 60}
}

;= TANK ======================================

{"tank_main-l"
	{uncover 10}
    ("vision_optic" args 40)
	{h_fov 20}
	{v_fov 140}
	{wide 2}
}

{"tank_main-m"
	{uncover 10}
    ("vision_optic" args 50)
	{h_fov 20}
	{v_fov 140}
	{wide 2}
}

{"tank_main-h"
	{uncover 10}
    ("vision_optic" args 55)
	{h_fov 20}
	{v_fov 140}
	{wide 2.5}
}

{"tank_driver"
	{uncover 10}
    ("vision_optic" args 20)
	{h_fov 100}
	{v_fov 90}
}

{"tank_back"
    ("vision_optic" args 20)
	{h_fov 80}
	{v_fov 120}
}

{"tank_around-l"
    ("human_stand" args 25)
	{h_fov 360}
	{v_fov 170}
}

{"tank_around-m"
    ("human_stand" args 20)
	{h_fov 360}
	{v_fov 170}
}

{"tank_around-h"
    ("human_stand" args 20)
	{h_fov 360}
	{v_fov 170}
}
