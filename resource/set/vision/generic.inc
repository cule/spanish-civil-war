(define "vision_optic"
	{radius %0}
	{rules
		{human 0.4
			{firing 0.4}
			{tankman2 0.7}
			{kamikaze 0.3}
			{cover 0.3
				{kamikaze 0.25}
				{sniper 0.15}
				{stealth 0.1}
				{stand 0.3}
				{squat 0.25}
				{crawl 0.2}
			}
			{squat 0.3
				{kamikaze 0.25}
				{sniper 0.25}
				{stealth 0.25}
			}
			{crawl 0.2
				{still 0.15
					{kamikaze 0.1}
					{sniper 0.1}
					{stealth 0.05}
				}
				{kamikaze 0.15}
				{sniper 0.15}
				{stealth 0.1}
			}
			{still 0.35}
		}

		{cannon_01 0.8			;heavy mgun, infantry mortar
			{firing 1}
			{visible 0.9}
			{still 0.8}
		}
		{cannon_02 0.5    		;light gun
			{firing 1.1}
			{visible 0.8}
			{still 0.45}
		}
		{cannon_03 0.7			;middle gun
			{firing 1.1}
			{visible 0.9}
			{still 0.6}
		}
		{cannon_04 0.8			;heavy gun
			{firing 1.3}
			{visible 1.2}
			{still 0.7}
		}
		{cannon_05 1			;howitzer
			{firing 1.5}
			{visible 1.5}
			{still 0.8}
		}
		{vehicle_00 0.5			;moto
			{firing 0.6}
			{visible 0.7}
			{still 0.45}
		}
		{vehicle_01 0.65		;jeep
			{firing 0.6}
			{visible 0.9}
			{still 0.55}
		}
		{vehicle_02 0.7			;light armoredcars
			{firing 0.95}
			{visible 1}
			{still 0.6}
		}
		{vehicle_03 0.75		;light SPA, armoredcars, trucks
			{firing 1.3}
			{visible 1.1}
			{still 0.65}
		}
		{vehicle_04 0.8			;light tanks
			{firing 1.3}
			{visible 1.3}
			{still 0.7}
		}
		{vehicle_04_spa 0.9		;middle SPA
			{firing 1.3}
			{visible 1.4}
			{still 0.8}
		}
		{vehicle_05 1			;middle tanks
			{firing 1.3}
			{visible 1.4}
			{still 0.9}
		}
		{vehicle_06 1.1			;heavy tanks
			{firing 1.4}
			{visible 1.5}
			{still 1}
		}
		{vehicle_07 0.75		;rocket artillery
			{firing 1.5}
			{visible 1.1}
			{still 0.65}
		}
	}
	{mods
		{stand 1}
	}
)

(define "vision_tankman"
	{radius %0}
	{rules
		{human 1}

		{cannon_01 1}
		{cannon_02 1}
		{cannon_03 1}
		{cannon_04 1}
		{cannon_05 1}
		{vehicle_00 1}
		{vehicle_01 1}
		{vehicle_02 1}
		{vehicle_03 1}
		{vehicle_04 1}
		{vehicle_04_spa 1}
		{vehicle_05 1}
		{vehicle_06 1}
		{vehicle_07 1}
	}
)


(define "vision_human"
	{radius %0}
	{rules
		{human 1
			{firing 1.2
				{firing_silent 0.1}
				{firing_delisle 0.2}
				{knife_ger 0.1}
				{knife_rus 0.1}
				{knife_union 0.1}
				{f1 0.1}
				{m18 0.1}
				{m24 0.1}
				{m24x5 0.1}
				{m39 0.1}
				{m61 0.1}
				{mk1 0.1}
				{molotov_coctail 0.1}
				{n73at 0.1}				
				{pwm1 0.1}
				{rg42x3 0.1}
				{rpg40 0.1}
				{rpg43 0.1}
				{smoke 0.1}
				{type_3_at 0.1}
				{type_97 0.1}
				{squat 1.4}
				{crawl 1.3}
				{cover 1.3}
			}
			{cover 0.6
				{kamikaze 0.4}
				{sniper 0.4}
				{stealth 0.3}
				{stand 0.6}
				{squat 0.5}
				{crawl 0.5}
			}
			{squat 0.7
				{sniper 0.5}
				{stealth 0.5}
			}
			{crawl 0.8
				{still 0.7
					{kamikaze 0.4}
					{sniper 0.35}
					{stealth 0.25}
				}
				{kamikaze 0.5}
				{sniper 0.45}
				{stealth 0.4}
			}
			{still 0.8}
		}

		{cannon_01 1			;heavy mgun, infantry mortar
			{firing 1.3}
			{visible 1.1}
			{still 0.8}
		}
		{cannon_02 1.1    		;light gun
			{firing 1.4}
			{visible 1.2}
			{still 0.9}
		}
		{cannon_03 1.2			;middle gun
			{firing 1.5}
			{visible 1.3}
			{still 1}
		}
		{cannon_04 1.3			;heavy gun
			{firing 1.6}
			{visible 1.4}
			{still 1.1}
		}
		{cannon_05 1.4			;howitzer
			{firing 1.7}
			{visible 1.5}
			{still 1.2}
		}
		{vehicle_00 1			;moto
			{firing 1.6}
			{visible 1.3}
			{still 0.9}
		}
		{vehicle_01 1.1			;jeep
			{firing 1.6}
			{visible 1.4}
			{still 1}
		}
		{vehicle_02 1.2			;light armoredcars
			{firing 1.6}
			{visible 1.4}
			{still 1}
		}
		{vehicle_03 1.3			;light SPA, armoredcars, trucks
			{firing 1.8}
			{visible 1.3}
			{still 1.1}
		}
		{vehicle_04 1.5			;light tanks
			{firing 1.8}
			{visible 1.7}
			{still 1.3}
		}
		{vehicle_04_spa 1.5		;middle SPA
			{firing 1.8}
			{visible 1.3}
			{still 1.4}
		}
		{vehicle_05 1.8			;middle tanks
			{firing 2}
			{visible 1.8}
			{still 1.6}
		}
		{vehicle_06 2			;heavy tanks
			{firing 2.3}
			{visible 2}
			{still 2}
		}
		{vehicle_07 2.1			;rocket artillery
			{firing 3}
			{visible 2}
			{still 2}
		}
	}
	{mods
		{stand 1}
	}
)

(define "vision_elite"
	{radius %0}
	{rules
		{human 1
			{firing 1.3
				{firing_silent 0.1}
				{firing_delisle 0.2}
				{knife_ger 0.1}
				{knife_rus 0.1}
				{knife_union 0.1}
				{f1 0.1}
				{m18 0.1}
				{m24 0.1}
				{m24x5 0.1}
				{m39 0.1}
				{m61 0.1}
				{mk1 0.1}
				{molotov_coctail 0.1}
				{n73at 0.1}				
				{pwm1 0.1}
				{rg42x3 0.1}
				{rpg40 0.1}
				{rpg43 0.1}
				{smoke 0.1}
				{type_3_at 0.1}
				{type_97 0.1}
				{squat 1.4}
				{crawl 1.3}
				{cover 1.3}
			}
			{cover 0.8
				{kamikaze 0.4}
				{sniper 0.4}
				{stealth 0.3}
				{stand 0.6}
				{squat 0.5}
				{crawl 0.5}
			}
			{squat 0.9
				{sniper 0.5}
				{stealth 0.5}
			}
			{crawl 0.9
				{still 0.7
					{kamikaze 0.4}
					{sniper 0.35}
					{stealth 0.25}
				}
				{kamikaze 0.5}
				{sniper 0.45}
				{stealth 0.4}
			}
			{still 1}
		}

		{cannon_01 1			;heavy mgun, infantry mortar
			{firing 1.3}
			{visible 1.1}
			{still 0.8}
		}
		{cannon_02 1.1    		;light gun
			{firing 1.4}
			{visible 1.2}
			{still 0.9}
		}
		{cannon_03 1.2			;middle gun
			{firing 1.5}
			{visible 1.3}
			{still 1}
		}
		{cannon_04 1.3			;heavy gun
			{firing 1.6}
			{visible 1.4}
			{still 1.1}
		}
		{cannon_05 1.4			;howitzer
			{firing 1.7}
			{visible 1.5}
			{still 1.2}
		}
		{vehicle_00 1			;moto
			{firing 1.6}
			{visible 1.3}
			{still 0.9}
		}
		{vehicle_01 1.1			;jeep
			{firing 1.6}
			{visible 1.4}
			{still 1}
		}
		{vehicle_02 1.2			;light armoredcars
			{firing 1.6}
			{visible 1.4}
			{still 1}
		}
		{vehicle_03 1.3			;light SPA, armoredcars, trucks
			{firing 1.8}
			{visible 1.3}
			{still 1.1}
		}
		{vehicle_04 1.5			;light tanks
			{firing 1.8}
			{visible 1.7}
			{still 1.3}
		}
		{vehicle_04_spa 1.5		;middle SPA
			{firing 1.8}
			{visible 1.3}
			{still 1.4}
		}
		{vehicle_05 1.8			;middle tanks
			{firing 2}
			{visible 1.8}
			{still 1.6}
		}
		{vehicle_06 2			;heavy tanks
			{firing 2.3}
			{visible 2}
			{still 2}
		}
		{vehicle_07 2.1			;rocket artillery
			{firing 3}
			{visible 2}
			{still 2}
		}
	}
	{mods
		{stand 1}
	}
)

(define "vision_officer"
	{radius %0}
	{rules
		{human 1
			{firing 1.5
				{firing_silent 0.1}
				{firing_delisle 0.2}
				{knife_ger 0.1}
				{knife_rus 0.1}
				{knife_union 0.1}
				{f1 0.1}
				{m18 0.1}
				{m24 0.1}
				{m24x5 0.1}
				{m39 0.1}
				{m61 0.1}
				{mk1 0.1}
				{molotov_coctail 0.1}
				{n73at 0.1}				
				{pwm1 0.1}
				{rg42x3 0.1}
				{rpg40 0.1}
				{rpg43 0.1}
				{smoke 0.1}
				{type_3_at 0.1}
				{type_97 0.1}
				{squat 1.4}
				{crawl 1.3}
				{cover 1.3}
			}
			{cover 0.85
				{stand 0.7}
				{squat 0.7}
				{crawl 0.7}
				{stealth 0.7}
			}
			{squat 0.85
				{sniper 0.7}
				{stealth 0.7}
			}
			{crawl 0.75
				{still 0.6}
				{sniper 0.7}
				{stealth 0.6}
			}
			{still 0.8}
		}

		{cannon_01 1.5			;heavy mgun, infantry mortar
			{firing 2}
			{visible 2}
			{still 1.3}
		}
		{cannon_02 1.5    		;light gun
			{firing 1.8}
			{visible 1.8}
			{still 1.4}
		}
		{cannon_03 1.5			;middle gun
			{firing 2}
			{visible 2}
			{still 1.5}
		}
		{cannon_04 1.7			;heavy gun
			{firing 2}
			{visible 2}
			{still 1.7}
		}
		{cannon_05 2			;howitzer
			{firing 2.5}
			{visible 2.5}
			{still 2}
		}
		{vehicle_00 1.4			;moto, jeep
			{firing 1.7}
			{visible 1.7}
			{still 1.1}
		}
		{vehicle_01 2			;moto, jeep
			{firing 2.1}
			{visible 2.1}
			{still 2}
		}
		{vehicle_02 1.5			;light armoredcars
			{firing 2.1}
			{visible 2.1}
			{still 1.5}
		}
		{vehicle_03 1.5			;light SPA, armoredcars, trucks
			{firing 2}
			{visible 2}
			{still 1.5}
		}
		{vehicle_04 1.8			;light tanks
			{firing 2.2}
			{visible 2.2}
			{still 1.8}
		}
		{vehicle_04_spa 2		;middle SPA
			{firing 2.3}
			{visible 2.3}
			{still 2}
		}
		{vehicle_05 2.1			;middle tanks
			{firing 2.4}
			{visible 2.4}
			{still 2.1}
		}
		{vehicle_06 2.2			;heavy tanks
			{firing 2.5}
			{visible 2.5}
			{still 2.2}
		}
		{vehicle_07 2			;rocket artillery
			{firing 3}
			{visible 3}
			{still 2}
		}
	}
	{mods
		{stand 1}
	}
)

(define "glass"
	{radius %0}
)

(define "human_stand"
		{radius %0}
	{rules
		{human 0.75
			{tankman2 1.5}
			{firing 1
				{firing_silent 0.2}
				{firing_delisle 0.2}
				{knife_ger 0.1}
				{knife_rus 0.1}
				{knife_union 0.1}
				{f1 0.1}
				{m18 0.1}
				{m24 0.1}
				{m24x5 0.1}
				{m39 0.1}
				{m61 0.1}
				{mk1 0.1}
				{molotov_coctail 0.1}
				{n73at 0.1}				
				{pwm1 0.1}
				{rg42x3 0.1}
				{rpg40 0.1}
				{rpg43 0.1}
				{smoke 0.1}
				{type_3_at 0.1}
				{type_97 0.1}

				{squat 1.4}
				{crawl 1.2}
				{cover 1.2}
			}
			{crawl 0}
			{cover 0}
			{squat 0}
			{stealth 0.45}
			{still 0.55}
		}

		{cannon_01 1.2			;�⠭�.�㫥����, ��������
			{firing 1.5}
			{still 1}
		}
		{cannon_02 1.5    		;������ ��誨, ...
			{firing 3}
			{still 1.2}
		}
		{cannon_03 2			;�।��� ��誨
			{firing 3.5}
			{still 1.75}
		}
		{cannon_04 2.5			;�殮�� ��誨
			{firing 4}
			{still 2}
		}
		{cannon_05 3			;��㡨��
			{firing 5}
			{still 2.5}
		}
		{vehicle_00 1.5
			{firing 2.5}
			{still 1.3}
		}
		{vehicle_01 2
			{firing 3}
			{still 1.75}
		}
		{vehicle_02 2.5
			{firing 3}
			{still 2}
		}
		{vehicle_03 2.75
			{firing 3.25}
			{still 2.25}
		}
		{vehicle_04 3
			{firing 3.5}
			{still 2.25}
		}
		{vehicle_04_spa 3		;�।��� ��
			{firing 3.5}
			{still 2.5}
		}
		{vehicle_05 3.25
			{firing 5}
			{still 2.75}
		}
		{vehicle_06 3.5
			{firing 4}
			{still 3}
		}
		{vehicle_07 3
			{firing 5}
			{still 2.25}
		}
	}
	{mods
		{stand 1}
		{kamikaze 0.8}
	}
)

{"cover-detector"
	{uncover 5}
	{h_fov 60}
	; {wide 2.5}	; don't use wide --> this lead to wrong results, as h_fov is increased to 360
	{v_fov 120}
	("vision_optic" args 50)
}

{"animal"
	{uncover 2}
	{h_fov 360}
	{v_fov 120}
}

{"searchlight"
	("vision_optic" args 60)
	{h_fov 30}
	{v_fov 90}
;	{wide 1.1}
}

{"watchtower"
    ("vision_optic" args 50)
	{h_fov 90}
	{v_fov 90}
;	{wide 2.5}
}

{"marine_main"
    ("vision_optic" args 24)
	{h_fov 50}
;	{wide 2}
	{v_fov 120}
}

;= MAP HELPER ====================================

(define "ultra_vision"
	{radius 20}
	{h_fov 360}
   	{v_fov 180}
	{uncover %range}
)
{"start_point"  ("ultra_vision" range(0))}
{"map_point"    ("ultra_vision" range(0))}
{"flag_point"   ("ultra_vision" range(0))}
