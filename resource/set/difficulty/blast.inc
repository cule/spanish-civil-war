	{cover_efficiency			; how cover efficiency will drop with distance
		0		0
		5		0.15
		15		0.7
		25		1
	}

	{blast_cover 2}           	; in cover from protected side
	{blast_crawl 2}				; simply lying on the ground
	{blast_crawl_prepared 2.5}	; lying on the ground & waiting for explosion
	{blast_cover_prepared 1.5}	; in cover not from protected side, but waiting for explosion
	{blast_squat 1.2}			; on the knees
